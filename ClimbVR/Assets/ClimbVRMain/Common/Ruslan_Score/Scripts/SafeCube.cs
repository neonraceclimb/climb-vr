﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeCube : MonoBehaviour {

    private Collider player;

    [SerializeField] private Transform endPoint;

    [SerializeField] private float timeOfPlayerMoving;

    [SerializeField] private AnimationCurve curveSlow;
    [SerializeField] private AnimationCurve curveFast;

    [SerializeField]
    private float duration = 2f;
    private float time;
     
    private Vector3 startPos;
    //public float Duration { get { return duration; } set { duration = value; } }

    private bool inCube = false ;


    private void Start()
    {
        player = GameManager.Instance.Player.GetComponent<Collider>();
        if(timeOfPlayerMoving == 0f) { timeOfPlayerMoving = 5f; }

    }

    //private void Update()
    //{
    //    print("current time is - " + Time.timeScale);
    //}


    private void OnTriggerEnter(Collider other)
    {
        if(other == player)
        {
			StartCoroutine(ChangeTimeSlow());
			startPos = GameManager.Instance.Player.transform.position;
            time = 0f;
            inCube = true;
        }
        else { Debug.Log("Not the player touched the safe Zone"); }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other == player) { inCube = false; StartCoroutine(ChangeTimeFast()); }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other == player)
        {
            MovePlayer(timeOfPlayerMoving);
        }
    }


    private void MovePlayer(float speed)
    {
        time += Time.fixedUnscaledDeltaTime / timeOfPlayerMoving;
        GameManager.Instance.Player.transform.position = Vector3.Lerp(startPos, endPoint.position, time);
    }

    private IEnumerator ChangeTimeSlow()
    {
        float start = Time.realtimeSinceStartup;
        float x = 0f;
        float lastCall = start;
        while (true)
        {
            Time.timeScale = (1f - curveSlow.Evaluate(x));
            x += (Time.realtimeSinceStartup - lastCall) / duration;
            lastCall = Time.realtimeSinceStartup;
            yield return new WaitForSecondsRealtime(0.01f);
            if(x >= 1f) { break; }
        }
    }

    private IEnumerator ChangeTimeFast() 
    {
		float start = Time.realtimeSinceStartup;
		float x = 0f;
		float lastCall = start;
		while (true)
		{
			Time.timeScale = (0f + curveFast.Evaluate(x));
			x += (Time.realtimeSinceStartup - lastCall) / duration;
			lastCall = Time.realtimeSinceStartup;
			yield return new WaitForSecondsRealtime(0.01f);
            if (x >= 1f) { Time.timeScale = 1f; break; }
		}
    }

}
