﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lawa : MonoBehaviour {


    public float start_lava = 1.45f;
    public float speed_lava = 0.05f;
    [SerializeField] private float attractForce;

    private Rigidbody player;

    void Awake()
    {
        GameManager.Instance.SetLava(this);
    }

    void Start () {
        player = GameManager.Instance.Player.GetComponent<Rigidbody>();
	}
	
	
	void Update () {
		if(player.transform.position.y >= start_lava)
        {
            MoveLava();
        }
        AttractPlayer();

    }

    private void MoveLava()
    {
        transform.Translate(0f, speed_lava*Time.deltaTime, 0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other == player)
        {
            //GameManager.Instance.EndGame();
        }
    }

    private void AttractPlayer()
    {
        Vector3 attractDir = transform.position - player.position;
        float forceRelativeToDistance = (1f / (1f + attractDir.magnitude));
        attractDir.Normalize();
        player.AddForce(attractDir * attractForce * forceRelativeToDistance, ForceMode.Force);
    }
}
