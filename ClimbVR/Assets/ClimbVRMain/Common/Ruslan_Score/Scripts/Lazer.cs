﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lazer : MonoBehaviour
{

    [SerializeField] private Vector3 offset = new Vector3(0, 200, 0);

    [SerializeField] private GameObject lazer;

    [SerializeField] private float timeToAim;

    [SerializeField] private float waitTimeForShoot;

    [SerializeField] private Vector3 startLazerScale;

    [SerializeField] private Vector3 endLazerScale;


    // will be deleted
    [SerializeField] private Transform lawa;
    private float distanceFormLazerToLawa;

    private Transform playerTransform;

    private Collider playerCollider;

    //public Collider Player { get; set; }

    private void Start()
	{

        playerCollider = GameManager.Instance.Player.GetComponent<Collider>();

		lazer.SetActive(false);

		playerTransform = GameManager.Instance.Player.body;




		transform.position = playerTransform.position + offset;

		lawa.transform.position = playerTransform.position - offset;

		transform.position = playerTransform.position + offset;


		distanceFormLazerToLawa = transform.position.y - lawa.transform.position.y;

		StartCoroutine(AimStart(timeToAim));
    }


    private void Setup()
    {
		//playerTransform = GameManager.Instance.Player.body;
		//transform.position = PrizmManager.Instance.LastPrizm.position + offset;
		//StartCoroutine(AimStart(timeToAim));
    }

    private IEnumerator AimStart(float time) {
        
        lazer.SetActive(true);

        startLazerScale.y = distanceFormLazerToLawa / 2f;
        endLazerScale.y = distanceFormLazerToLawa / 2f;
        lazer.transform.position = new Vector3(transform.position.x, transform.position.y - distanceFormLazerToLawa / 2f, transform.position.z);

		lazer.transform.localScale = startLazerScale;

        float step = 0f;

		while (true)
		{
            step += Time.unscaledDeltaTime / time;
            lazer.transform.localScale = Vector3.Lerp(startLazerScale, endLazerScale, step);
            yield return new WaitForEndOfFrame();
            if (step >= 1) { StartCoroutine(WaitForShoot(waitTimeForShoot)); break; }
		}
    }


	private IEnumerator WaitForShoot(float time)
	{
		float startTime = Time.realtimeSinceStartup;
		while (true)
		{
			yield return new WaitForSecondsRealtime(time);
			float endTime = Time.realtimeSinceStartup - startTime;
			if (endTime >= time) { Shoot(); break; }
		}
	}


    private void Shoot()
	{
		CapsuleCollider lazerCollider = lazer.GetComponent<CapsuleCollider>();

        var colliders = Physics.OverlapCapsule(new Vector3(lazerCollider.transform.position.x, lazerCollider.center.y - lazerCollider.height / 2f , lazerCollider.transform.position.z), 
                                               new Vector3(lazerCollider.transform.position.x, lazerCollider.center.y + lazerCollider.height / 2f , lazerCollider.transform.position.z),
                                               lazerCollider.radius);
        foreach (var collider in colliders){
            if (collider == playerCollider)
            {
                print("The player in lazer");
                // do some code
            }
        }
    }



}
