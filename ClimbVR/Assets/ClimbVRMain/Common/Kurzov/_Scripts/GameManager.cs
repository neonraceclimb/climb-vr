﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : GenericSingletonClass<GameManager> {

	#region Public
    public string WinScene;
    public string DefScene;
    //public string PouseScene;

    public bool StopGame;
    public bool isVictory;
    public bool isDefeate;
    public int PlayerHealth = 1;
	#endregion

    //PlayerController PlayerController;
    private Character m_Player;
	private Lawa m_Lava;
	//private bool isPause = false;

	public Character Player { get { return m_Player; } }
	public Lawa Lava { get { return m_Lava; } }
   
    // Use this for initialization
    void Start ()
    {
        //m_Player = GameObject.FindGameObjectWithTag("Player");
        //PlayerController = Player.GetComponent<PlayerController>();
    }

    public void EndGame()
    {
        SceneManager.LoadScene(DefScene);
        //DefPanel.SetActive(true);
        //Cursor.visible = true;
        //PlayerController.enabled = false;
    }
    public void WinGame()
    {
        SceneManager.LoadScene(WinScene);
        //WinPanel.SetActive(true);
        //Cursor.visible = true;
        //PlayerController.enabled = false;
    }
    // Update is called once per frame
    void Update ()
    {
        
    }

	public void SetPlayer(Character player)
	{
		if (m_Player == null) {
			m_Player = player;
		}
	}

	public void SetLava(Lawa lava)
	{
		if (m_Lava == null) {
			m_Lava = lava;
		}
	}
}
