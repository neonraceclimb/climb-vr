﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBarrier : MonoBehaviour 
{

	public Transform BlokBarriers1;
	public Transform BlokBarriers2;
	public GameObject Barrier;
	public Transform BlokPolint;
	GameObject BB;

	[SerializeField]
	private float m_GoTime;
	[SerializeField]
	private float m_StopTime;

	public float Dist;
	// Use this for initialization

	void Start () 
	{

		StartCoroutine(WaitForSpawnBarrier(m_GoTime));

		Dist = Vector3.Distance (BlokBarriers1.position, BlokBarriers2.position);

		//SpawnEnergyBarrier ();
		/*Rigidbody rb = GetComponent<Rigidbody>();
		//BB = Instantiate(Barrier, BlokPolint.position, BlokPolint.rotation) as GameObject;
		//BB.transform.parent = BlokPolint;

		Vector3 SP = Vector3.Lerp(BlokBarriers1.position, BlokBarriers2.position, 0.5f);
		BB = Instantiate(Barrier, SP, BlokPolint.rotation) as GameObject;

		// Для Плэйна
		BB.transform.localScale = new Vector3 (BB.transform.localScale.x, BB.transform.localScale.y, Dist*0.1f);

		// Для куба.
		//BB.transform.localScale = new Vector3 (BB.transform.localScale.x, BB.transform.localScale.y, Dist);

		BB.transform.parent = BlokPolint;
		BB.transform.LookAt (BlokBarriers1);*/

	}

	// Update is called once per frame
	void Update () 
	{
		//Dist = Vector3.Distance (BlokBarriers1.position, BlokBarriers2.position);
	}
		

	public void SpawnEnergyBarrier()
	{
		Rigidbody rb = GetComponent<Rigidbody>();
		//BB = Instantiate(Barrier, BlokPolint.position, BlokPolint.rotation) as GameObject;
		//BB.transform.parent = BlokPolint;

		Vector3 SP = Vector3.Lerp(BlokBarriers1.position, BlokBarriers2.position, 0.5f);
		BB = Instantiate(Barrier, SP, BlokPolint.rotation) as GameObject;

		// Для Плэйна
		BB.transform.localScale = new Vector3 (BB.transform.localScale.x, BB.transform.localScale.y, Dist*0.1f);

		// Для куба.
		//BB.transform.localScale = new Vector3 (BB.transform.localScale.x, BB.transform.localScale.y, Dist);

		BB.transform.parent = BlokPolint;
		BB.transform.LookAt (BlokBarriers1);

		StartCoroutine(WaitForOff(m_StopTime));
	}

	private IEnumerator WaitForSpawnBarrier(float time)
	{
		float startTime = Time.realtimeSinceStartup;
		while (true)
		{
			//do some code
			yield return new WaitForSecondsRealtime(time);
			float endTime = Time.realtimeSinceStartup - startTime;
			if (endTime >= time) 
			{ 
				print("Включаем барьер! +");
				SpawnEnergyBarrier();
				break; 
			}
		}
	}

	private IEnumerator WaitForOff(float time)
	{
		float startTime = Time.realtimeSinceStartup;
		while (true)
		{
			yield return new WaitForSecondsRealtime(time);
			float endTime = Time.realtimeSinceStartup - startTime;

			if (endTime >= time) 
			{ 
				print("Выключаем барьер! -");
				yield return new WaitForSeconds(0.5f);
				//yield return StartCoroutine(AnotherCoroutine());
				Destroy(BB);
				StartCoroutine(WaitForSpawnBarrier(m_GoTime));
				break;
			}
		}
	}
}
