﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAimController : MonoBehaviour {

    [SerializeField] private GameObject m_AimPrefab;

    private List<GameObject> m_SpawnedAims = new List<GameObject>();

    public static UIAimController instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            DestroyImmediate(this);
        }
    }

    public void ShowPossiblePlatforms(List<HarpoonPlatform> platforms)
    {
        foreach(HarpoonPlatform platform in platforms)
        {
            m_SpawnedAims.Add(Instantiate(m_AimPrefab, platform.transform.position, Quaternion.identity, platform.transform));
        }
    }

    public void HidePossiblePlatforms()
    {
        foreach(GameObject aim in m_SpawnedAims)
        {
            DestroyImmediate(aim);
        }

        m_SpawnedAims.Clear();
    }
}
