﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Harpoon : MonoBehaviour
{

    [SerializeField]
    private float m_HarpoonDistance;
    [SerializeField]
    private LineRenderer m_RayLineRend;
	[SerializeField]
	private LayerMask m_Layer;


    public Transform m_TargetTransform { get; private set; }

    public List<HarpoonPlatform> m_PossiblePlatforms { get; private set; }

    private void Awake()
    {
        m_PossiblePlatforms = new List<HarpoonPlatform>();
    }

    public void Aim(HandsController hand)
    {

        RaycastHit hit;
		if (Physics.Raycast (hand.transform.position, hand.transform.forward, out hit, m_HarpoonDistance, m_Layer)) {
			m_TargetTransform = hit.transform;
		} else {
			m_TargetTransform = null;
		}

		Debug.DrawRay (hand.transform.position, hand.transform.transform.forward, Color.green);


        UpdateView(hand);
    }

    public void SwitchOff()
    {
        m_RayLineRend.enabled = false;
        m_TargetTransform = null;

        m_PossiblePlatforms.RemoveAll(p => p != null);
        m_PossiblePlatforms.Clear();

        UIAimController.instance.HidePossiblePlatforms();
    }

	public void SwitchOn(HandsController hand)
	{
        m_RayLineRend.enabled = true;

        Collider[] possiblePlatformsColliders = Physics.OverlapSphere(hand.transform.position, m_HarpoonDistance, m_Layer);//, LayerMask.NameToLayer("AimPlatform"));
        //print(possiblePlatformsColliders.Length);
        foreach(Collider col in possiblePlatformsColliders)
        {
            HarpoonPlatform platform = col.GetComponent<HarpoonPlatform>();
            if(platform != null) m_PossiblePlatforms.Add(platform);
        }

        UIAimController.instance.ShowPossiblePlatforms(m_PossiblePlatforms);
    }

    private void UpdateView(HandsController hand)
    {
		Vector3 targetPos;
		if (m_TargetTransform != null) {
			targetPos = m_TargetTransform.position;
		} else {
			targetPos = hand.transform.position + hand.transform.forward * m_HarpoonDistance;
		}

        m_RayLineRend.SetPosition(0, hand.transform.position);
        m_RayLineRend.SetPosition(1, targetPos);
    }
}
