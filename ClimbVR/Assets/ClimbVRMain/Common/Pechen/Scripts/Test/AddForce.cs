﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForce : MonoBehaviour {

    [SerializeField]
    private Transform m_TargetTransform;
    [SerializeField]
    private float m_Force;

    private Rigidbody m_Rb;
	
    private void Start()
    {
        m_Rb = GetComponent<Rigidbody>();
    }

	// Update is called once per frame
	void FixedUpdate () {

        m_Rb.AddForce((m_TargetTransform.position - transform.position).normalized * m_Force);

	}
}
