﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarpoonPlatform : PlatformBase
{
    public override void Awake()
    {
        base.Awake();
        gameObject.layer = LayerMask.NameToLayer("AimPlatform");
    }

    public override void OnTriggerDown(HandsController controller)
    {
        base.OnTriggerDown(controller);
    }

    public override void OnPressTrigger(HandsController controller)
    {
        base.OnPressTrigger(controller);
    }

    public override void OnTriggerUp(HandsController controller)
    {
        base.OnTriggerUp(controller);
    }
}
