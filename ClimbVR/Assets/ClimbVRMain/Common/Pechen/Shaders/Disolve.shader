﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Unlit/Hologram"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_GridFrequency("Grid Frequency", Float) = 1
		_GridSize("Grid Size", Range(0, 1)) = 0.9
		_GridColor("Gird Color", Color) = (1, 1, 1, 1)
		_BottomColor("Bottom Color", Color) = (1, 1, 1, 1)
		_GradientOffset("Gradient Offset", Float) = 0
		_GradientScale("Gradient Scale", Float) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue" = "Transparent"}
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100

		Pass
		{
			Tags {
			   "LightMode" = "ForwardBase"
			}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 position : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float3 originPos : TEXCOORD3;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			half4 _MainTex_TexelSize;
			float _GridFrequency;
			float _GridSize;
			float4 _GridColor;
			float4 _BottomColor;
			float _GradientOffset;
			float _GradientScale;

			v2f vert (appdata v)
			{
				v2f i;
				i.worldPos = mul(unity_ObjectToWorld, v.position);
				i.originPos = mul(unity_ObjectToWorld, float4(0, 0, 0, 1)).xyz;
				i.position = UnityObjectToClipPos(v.position);
				i.normal = UnityObjectToWorldNormal(v.normal);

				i.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return i;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				//fixed4 col = tex2D(_MainTex, i.uv);
				i.normal = normalize(i.normal);
				float diffuse = dot(_WorldSpaceLightPos0.xyz, i.normal) / 2 + 0.7;

				float gridX = frac(i.position.x / 700 * _GridFrequency);
				float gridY = frac(i.position.y / 400 * _GridFrequency);
				gridX = step(_GridSize, gridX);
				gridY = step(_GridSize, gridY);

				float4 col = lerp(_BottomColor, _GridColor, saturate(i.worldPos.y * _GradientScale - i.originPos.y + _GradientOffset));

				return  diffuse * (gridX * gridY) * col;
			}
			ENDCG
		}
	}
}
