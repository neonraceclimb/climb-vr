﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BHImageEffect : MonoBehaviour {
    public Shader shader;

    public float radius = 0;    //Радиус черной дыры измеряемый в тех же единицах, что и остальные объекты на сцене

    public GameObject BH;  //Объект, позиция которого берется за позицию черной дыры

    private Material _material; //Материал на котором будет находится шейдер
    private float ratio;     //Отношение высоты к длине экрана, для правильного отображения шейдера
    private Camera m_Camera;

    protected Material material
    {
        get
        {
            if (_material == null)
            {
                _material = new Material(shader);
                _material.hideFlags = HideFlags.HideAndDontSave;
            }
            return _material;
        }
    }

    private void Start()
    {
        m_Camera = GetComponent<Camera>();
        ratio = m_Camera.aspect;
    }

    protected virtual void OnDisable()
    {
        if (_material)
        {
            DestroyImmediate(_material);
        }
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (shader && material)
        {
            //Находим позицию черной дыры в экранных координатах
            Vector2 pos = new Vector2(
                m_Camera.WorldToScreenPoint(BH.transform.position).x / m_Camera.pixelWidth,
                1 - m_Camera.WorldToScreenPoint(BH.transform.position).y / m_Camera.pixelHeight);

            //Устанавливаем все необходимые для шейдера параметры
            material.SetVector("_Position", new Vector2(pos.x, pos.y));
            material.SetFloat("_Ratio", ratio);
            material.SetFloat("_Rad", radius);
            material.SetFloat("_Distance", Vector3.Distance(BH.transform.position, this.transform.position));
            //И применяем к полученному изображению.
            Graphics.Blit(source, destination, material);
        }
    }
}
