﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrismaBase : MonoBehaviour {

    [SerializeField] protected float m_PushOutForce;
    [SerializeField, Range(0f, 1f)] protected float m_PushOutDamp;
    protected Rigidbody m_PlayerBody;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        print("OnTriggerEnter");
        if (other.gameObject.tag == "Player")
        {
            m_PlayerBody = other.gameObject.GetComponent<Rigidbody>();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        print("OnTriggerStay");
        if (m_PlayerBody != null)
        {
            Vector3 force = new Vector3(m_PlayerBody.position.x, 0f, m_PlayerBody.position.z) - new Vector3(transform.position.x, 0f, transform.position.z);

            m_PlayerBody.AddForce(force, ForceMode.Force);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        print("OnTriggerExit");
        if (m_PlayerBody != null)
        {
            m_PlayerBody.AddForce((-m_PlayerBody.velocity / Time.fixedDeltaTime) / (1f + m_PushOutDamp), ForceMode.Force);

            m_PlayerBody = null;
        }
    }
}
