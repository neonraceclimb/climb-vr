﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveInputWrapper : MonoBehaviour {

    [SerializeField] private SteamVR_TrackedObject m_TrackedObj;

    private HandsController m_HandsController;

    private SteamVR_Controller.Device Controller { get { return SteamVR_Controller.Input((int)m_TrackedObj.index); } }

    private void Start()
    {
        m_HandsController = GetComponent<HandsController>();
    }

    private void Update()
    {

        if (m_TrackedObj.index != SteamVR_TrackedObject.EIndex.None)
        {
            if (Controller.GetHairTriggerDown())
            {
                m_HandsController.PressTriggerDown();
            }
            if (Controller.GetHairTrigger())
            {
                m_HandsController.PressTrigger();
            }
            if (Controller.GetHairTriggerUp())
            {
                m_HandsController.PressTriggerUp();
            }
        }
    }

    private void FixedUpdate()
    {
        Vector3 localPosition = m_TrackedObj.transform.localPosition;
        Quaternion localRotation = m_TrackedObj.transform.localRotation;
        m_HandsController.TrackPosition(localPosition, localRotation);
    }
}
