﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInputWrapper : MonoBehaviour {

    [SerializeField] private KeyCode m_GrabButton;
    [SerializeField] private KeyCode m_AimButton;
    [SerializeField] private Vector3 m_HandOffset;

    private HandsController m_HandsController;

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        m_HandsController = GetComponent<HandsController>();
    }

    void Update () {
        if (Input.GetKeyDown(m_GrabButton))
        {
            m_HandsController.PressTriggerDown();
        }
        if (Input.GetKey(m_GrabButton))
        {
            m_HandsController.PressTrigger();
        }
        if (Input.GetKeyUp(m_GrabButton))
        {
            m_HandsController.PressTriggerUp();
        }

        if (Input.GetKeyDown(m_AimButton))
        {
            m_HandsController.PressAimButtonDown();
        }
        else if (Input.GetKey(m_AimButton))
        {
            m_HandsController.PressAimButton();
        }
        else if (Input.GetKeyUp(m_AimButton))
        {
            m_HandsController.PressAimButtonUp();
        }

        Vector3 playerOffset = GameManager.Instance.Player.transform.position;
        Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f)) + m_HandOffset - playerOffset;
        Quaternion rotation = Quaternion.identity;
        m_HandsController.TrackPosition(position, rotation);
    }
}
