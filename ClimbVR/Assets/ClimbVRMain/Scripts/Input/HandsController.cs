﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HandStates { FreeMove, GrabLedge, Aim, HarpoonFly }

public class HandsController : MonoBehaviour
{

    [SerializeField]
    private Transform m_SnapPoint;
    [SerializeField]
    private float m_OverlapSphereRadius;
    [SerializeField]
    private float m_HandSpeed;

    private Character m_Player;
    private Harpoon m_Harpoon;
    private bool m_IsTrackedPos;
    private bool m_AimIsSetup;

    private Coroutine m_HandCoroutine;

    public PlatformBase m_CurrentPlatform { get; private set; }
    public Transform m_CurrentLedge { get; private set; }

    public Vector3 m_ControllerLocalPos { get; private set; }
    public Quaternion m_ControllerLocalRot { get; private set; }

    public Vector3 m_PressedHandLocalPosition { get; private set; }
    public Vector3 m_PressedPlayerPosition { get; private set; }
    public Vector3 m_PressedPlayerLocalPosition { get; private set; }

    private void Awake()
    {
        m_AimIsSetup = false;
        m_IsTrackedPos = true;
    }

    private void Start()
    {
        m_Harpoon = GetComponent<Harpoon>();
        m_Player = GameManager.Instance.Player;
    }

    private bool CheckPlatform()
    {
        Collider[] overlapedColliders = Physics.OverlapSphere(m_SnapPoint.position, m_OverlapSphereRadius);

        List<Collider> possibleColliders = new List<Collider>();

        foreach (Collider col in overlapedColliders)//find free platforms
        {
            if (col.CompareTag("Platform") && col.GetComponent<PlatformBase>().GetFreeLedges().Count != 0)
            {
                possibleColliders.Add(col);
            }
        }

        Collider nearest = null;
        float minDist = Mathf.Infinity;
        foreach (Collider col in possibleColliders)//find nearest platform
        {
            float dist = (transform.position - col.transform.position).sqrMagnitude;
            if (dist < minDist)
            {
                nearest = col;
                minDist = dist;
            }
        }

        if (nearest != null)
        {
            m_CurrentPlatform = nearest.GetComponent<PlatformBase>();
            m_CurrentLedge = m_CurrentPlatform.GetNearestLedge(this.transform);
            return true;
        }

        return false;
    }

    public void PressTriggerDown()
    {
        if (m_Player.m_CurrentState == PlayerStates.Aim)
        {
            if (m_Harpoon.m_TargetTransform != null)
            {
                m_Player.MoveWithHarpoon(this, m_Harpoon.m_TargetTransform);
                m_Harpoon.SwitchOff();
                GameManager.Instance.Player.SetEnergy(-1);
            }
        }
        else
        {
            if ((m_Player.m_CurrentState == PlayerStates.FreeMove || m_Player.m_CurrentState == PlayerStates.GrabLedge) && CheckPlatform())
            {
                GrabPlatform();

                m_CurrentPlatform.OnTriggerDown(this);
                m_Player.OnGrabPlatform(this);
            }
        }
    }

    public void PressTrigger()
    {
        if (m_CurrentPlatform != null && m_CurrentLedge != null)
        {
            m_CurrentPlatform.OnPressTrigger(this);
            m_Player.OnKeepPlatform(this);
        }
    }

    public void PressTriggerUp()
    {
        if (m_CurrentPlatform != null)
        {
            m_CurrentPlatform.OnTriggerUp(this);
            m_Player.OnReleasePlatform(this);

            ReleasePlatform();
        }
        else if (m_Player.m_CurrentState == PlayerStates.HarpoonFly)
        {
            m_Player.StopMoveWithHarpoon();
        }
    }

    public void TriggerUp()// Add invoke logic
    {
        PressTriggerUp();
    }

    public void PressAimButtonDown()
    {
        if (m_Player.m_CurrentState == PlayerStates.FreeMove && GameManager.Instance.Player.EnergyCount >= 1)
        {
            m_AimIsSetup = true;
            m_Harpoon.SwitchOn(this);
            m_Player.OnAimDown();
        }
    }

    public void PressAimButton()
    {
        if (m_AimIsSetup && m_Player.m_CurrentState == PlayerStates.Aim)
        {
            m_Harpoon.Aim(this);
        }
    }

    public void PressAimButtonUp()
    {
        if (m_AimIsSetup && m_Player.m_CurrentState == PlayerStates.Aim)
        {
            m_Harpoon.SwitchOff();
            m_Player.OnAimUp();
            m_AimIsSetup = false;
        }
    }

    private void GrabPlatform()
    {
        m_PressedHandLocalPosition = m_ControllerLocalPos;
        m_PressedPlayerPosition = m_Player.transform.position;

        transform.parent = m_CurrentLedge.transform;

        if (m_CurrentPlatform.m_ParentPrisma != null) m_Player.SetParent(m_CurrentPlatform.m_ParentPrisma.transform);
        m_PressedPlayerLocalPosition = m_Player.transform.localPosition;

        m_IsTrackedPos = false;
        m_HandCoroutine = StartCoroutine(GrabPlatformSmoothly());
    }

    private void ReleasePlatform()
    {
        if (m_HandCoroutine != null)
        {
            StopCoroutine(m_HandCoroutine);
            m_HandCoroutine = null;
        }

        m_Player.SetParent(null);
        transform.parent = m_Player.transform;
        m_IsTrackedPos = true;

        m_CurrentLedge = null;
        m_CurrentPlatform = null;
    }

    public void TrackPosition(Vector3 localPosition, Quaternion localRotation)
    {
        m_ControllerLocalPos = localPosition;
        m_ControllerLocalRot = localRotation;
        if (m_IsTrackedPos)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, localPosition, m_HandSpeed * Time.fixedDeltaTime);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, localRotation, m_HandSpeed * Time.fixedDeltaTime);
        }
    }

    private IEnumerator GrabPlatformSmoothly()
    {
        float distance = float.PositiveInfinity;
        float speed = 15f;
        while(distance > 0.005f)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, 2 * Time.deltaTime);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.identity, 2 * Time.deltaTime);
            distance = (m_CurrentLedge.transform.localPosition - transform.localPosition).magnitude;
            yield return null;
        }

        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;

        StopCoroutine(m_HandCoroutine);
        m_HandCoroutine = null;
    }
}
