﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OculusControllerTracker : MonoBehaviour {

	[SerializeField] private OVRInput.Controller m_Controller;
	
	// Update is called once per frame
	void Update () {
		transform.localPosition = OVRInput.GetLocalControllerPosition(m_Controller);
		transform.localRotation = OVRInput.GetLocalControllerRotation(m_Controller);
	}
}
