﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerStates { FreeMove, GrabLedge, Aim, HarpoonFly }


public class Character : MonoBehaviour
{
    [SerializeField]
    private float m_PlayerSpeed;

    private Rigidbody m_Rb;
    private Coroutine m_CurrentCoroutine;
    private List<HandsController> m_TrackedHands = new List<HandsController>();
    public PlayerStates m_CurrentState { get; private set; }
    public int TrackedHandsCount { get { return m_TrackedHands.Count; } }

    private Vector3 m_Velocity;
    private Vector3 m_PrevPos;

    public Transform body {
        get { return transform; }
    }


    [SerializeField] private VisualizeEnergy visualizeLeftEnergy;
    [SerializeField] private VisualizeEnergy visualizeRightEnergy;

    private int energyCount = 0;
    public int EnergyCount { get { return energyCount; } set { energyCount = value; } }


    private void Awake()
    {
        energyCount = GV.energy;
        GameManager.Instance.SetPlayer(this);
    }

    private void Start()
    {

        energyCount = 0;

        m_Rb = GetComponent<Rigidbody>();

        m_CurrentState = PlayerStates.FreeMove;
    }

    private void Update()
    {
        switch (m_CurrentState)
        {
            case PlayerStates.FreeMove:
                //
                break;
            case PlayerStates.GrabLedge:
                transform.localPosition = Vector3.Lerp(transform.localPosition, CalculatePosition(), m_PlayerSpeed * Time.deltaTime);
                break;
            case PlayerStates.Aim:
                //
                break;
            case PlayerStates.HarpoonFly:
                //
                break;
        }

        CalculateVelocity();
    }

    public void OnGrabPlatform(HandsController hand)
    {
        EnablePhysics(false);

        m_TrackedHands.Add(hand);

        m_CurrentState = PlayerStates.GrabLedge;
    }

    public virtual void OnKeepPlatform(HandsController hand)
    {

    }

    public void OnReleasePlatform(HandsController hand)
    {
        m_TrackedHands.Remove(hand);

        if (m_TrackedHands.Count == 0)
        {
            EnablePhysics(true);

            m_Rb.AddForce(m_Velocity / Time.fixedDeltaTime, ForceMode.Impulse);

            m_CurrentState = PlayerStates.FreeMove;
        }
    }

    public void OnAimDown()
    {
        m_CurrentState = PlayerStates.Aim;
    }

    public void OnAimUp()
    {
        m_CurrentState = PlayerStates.FreeMove;
    }

    private Vector3 CalculatePosition()
    {
        Vector3 finalPos = Vector3.zero;
        if (m_TrackedHands.Count == 1)
        {
            finalPos = m_TrackedHands[0].m_PressedPlayerLocalPosition + (m_TrackedHands[0].m_PressedHandLocalPosition - m_TrackedHands[0].m_ControllerLocalPos);
        }
        else if (m_TrackedHands.Count == 2)
        {
            Vector3 relativePos1 = m_TrackedHands[0].m_PressedPlayerLocalPosition + (m_TrackedHands[0].m_PressedHandLocalPosition - m_TrackedHands[0].m_ControllerLocalPos);
            Vector3 relativePos2 = m_TrackedHands[1].m_PressedPlayerLocalPosition + (m_TrackedHands[1].m_PressedHandLocalPosition - m_TrackedHands[1].m_ControllerLocalPos);
            finalPos = Vector3.Lerp(relativePos1, relativePos2, 0.5f);
        }
        else
        {
            Debug.LogWarning("TrakedHands > 2");
        }

        return finalPos;
    }

    public void MoveWithHarpoon(HandsController hand, Transform harpoonTarget)
    {
        EnablePhysics(false);

        m_CurrentState = PlayerStates.HarpoonFly;

        m_CurrentCoroutine = StartCoroutine(MoveToLedgeHarpoon(hand, harpoonTarget));
    }

    public void StopMoveWithHarpoon()
    {
        if (m_CurrentCoroutine != null) StopCoroutine(m_CurrentCoroutine);

        EnablePhysics(true);

        m_Rb.AddForce(m_Velocity / Time.fixedDeltaTime, ForceMode.Impulse);

        m_CurrentState = PlayerStates.FreeMove;
    }

    private void EnablePhysics(bool enable)
    {
        m_Rb.isKinematic = !enable;
        m_Rb.useGravity = enable;
    }

    private IEnumerator MoveToLedgeHarpoon(HandsController hand, Transform harpoonTarget)
    {
        Vector3 startPosition = transform.position;
        Vector3 finalPosition = harpoonTarget.position + (transform.position - hand.transform.position);//add offset

        float t = 0f;
        float distance = Vector3.Distance(finalPosition, transform.position);

        float speed = 3f / distance;//move to inspector

        while (distance > 0.2f)
        {
            t += Time.deltaTime * speed;
            transform.position = Vector3.Lerp(startPosition, finalPosition, t);// evaluate by curve

            distance = Vector3.Distance(finalPosition, transform.position);

            yield return null;
        }

        StopMoveWithHarpoon();

        hand.PressTriggerDown();
    }

    private void CalculateVelocity()
    {
        m_Velocity = transform.position - m_PrevPos;

        m_PrevPos = transform.position;
    }

    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
    }

    public void SetEnergy(int energy)
    {
        energyCount += energy;
		energyCount = Mathf.Clamp(energyCount, 0, 3);

        visualizeLeftEnergy.SetEnergy(energyCount);
		visualizeRightEnergy.SetEnergy(energyCount);

        GV.energy = energyCount;
    }


}
